package curso.umg.gt.ultimaprueba;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class UsuarioActivity extends AppCompatActivity {

    private EditText et1, et2, et3,et4;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.usuario_main);

        et1 = (EditText) findViewById(R.id.et1);
        et2 = (EditText) findViewById(R.id.et2);
        et3 = (EditText) findViewById(R.id.et3);
        et4 = (EditText) findViewById(R.id.et4);
    }

    public void login(View view) {

        String nombre = et1.getText().toString();
        String apellido = et2.getText().toString();
        String pass = et3.getText().toString();
        String confpass = et4.getText().toString();

        if (pass.equals(confpass)){

            Intent i = new Intent(this, InteresesActivity.class);
            startActivity(i);

            Toast notification = Toast.makeText(this, "ACCESO CORRECTO", Toast.LENGTH_SHORT);
            notification.show();
            et1.setText("");
            et2.setText("");
            et3.setText("");
            et4.setText("");

        }else {
            Toast notification = Toast.makeText(this, "contraseñas no son iguales", Toast.LENGTH_SHORT);
            notification.show();
            et3.setText("");
            et4.setText("");

        }
    }


    public void cancelar(View view){

        et1.setText("");
        et2.setText("");
        et3.setText("");
        et4.setText("");

    }
}
