package curso.umg.gt.ultimaprueba;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class InteresesActivity extends AppCompatActivity {

    private RadioButton rb1, rb2,rb3,rb5,rb6,rb7;
    private Button enviar;
    private List<String>Listado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intereses);

        Listado =new ArrayList<>();
        rb1=(RadioButton) findViewById(R.id.rb1);
        rb2=(RadioButton) findViewById(R.id.rb2);
        rb3=(RadioButton) findViewById(R.id.rb3);
        rb5=(RadioButton) findViewById(R.id.rb5);
        rb6=(RadioButton) findViewById(R.id.rb6);
        rb7=(RadioButton) findViewById(R.id.rb7);
    }

    public void enviar(View view){

        Toast passVer=Toast.makeText(this,"listas de interes :"+Listado.toString(), Toast.LENGTH_LONG);
        passVer.show();;
    }

    public void onRadioButtonClicked(View view){
    boolean check=((RadioButton)view).isChecked();

        switch (view.getId()){
            case R.id.rb1:
                if (check)
                    Listado.add("Android");
            break;
            case R.id.rb3:
                if (check)
                    Listado.add("Java");
            break;
            case R.id.rb6:
                if (check)
                    Listado.add("Swing");
                break;
        }
    }

    public void onRadioButtonClickedRemove(View view){
        boolean check=((RadioButton)view).isChecked();

        switch (view.getId()){
            case R.id.rb2:
                if (check)
                    Listado.remove("Android");
                break;
            case R.id.rb5:
                if (check)
                    Listado.remove("Java");
                break;
            case R.id.rb7:
                if (check)
                    Listado.remove("Android");
                break;
        }
    }


}
